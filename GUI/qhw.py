import tkinter as tk
import tkinter.messagebox as msg
import tkinter.filedialog
import time
from PIL import Image,ImageTk
import cv2

class MainFram(tk.Frame):
    def __init__(self, frame, width, height):
        super(MainFram, self).__init__(frame,width=width, height = height, cursor = 'arrow')
        # self.canvas = tk.Canvas(self, bg='yellow', height=100, width=200, bd = '10')
        # self.canvas.pack()
        self.label = tk.Label(self, text="欢迎来到faceswap换脸世界——采用AI换脸技术", padx=5, pady=5)
        self.label.pack()



class LeftFrm(tk.Frame):
    def __init__(self, frame):
        super(LeftFrm, self).__init__(frame)
        self.canvas = tk.Canvas(self, bg='yellow', height=800, width=400)
        self.canvas.pack()
        # self.user1Pic1 = tk.Canvas(self.canvas,height= 100, width = 200, bg = 'white').place(x=100, y = 50, anchor = 'nw')
        self.button1 = tk.Button(self.canvas, text='选择被换脸用户',command = self.select_user1).place(x=50, y=10, anchor='nw')
        self.button2 = tk.Button(self.canvas, text='选择换脸用户', command=self.select_user2).place(x=50, y=180, anchor='nw')
        # self.user1Pic2 = tk.Canvas(self.canvas, height=100, width=200, bg='white').place(x=100, y=220, anchor='nw')
        # self.button2 = tk.Button(self.canvas, text='生成换脸结果', command=self.genImage).place(x=100, y=340, anchor='nw')
        # self.label = tk.Label(self.canvas, text="选择图片", padx=5, pady=5)
        # self.label.pack()

    def hit_me(self):
        print(tk.messagebox.askquestion(title='Hi', message='请耐心等待'))

    def select_user1(self):
        global img,im
        filename = tk.StringVar()
        filepath = tkinter.filedialog.askopenfilename()  # 选择打开什么文件，返回文件名
        filename.set(filepath)  # 设置变量filename的值
        img = Image.open(filename.get())  # 打开图片
        img = img.resize((200, 150), Image.ANTIALIAS)
        im = ImageTk.PhotoImage(img)
        image = self.canvas.create_image(150, 30, anchor='nw', image=im)

    def select_user2(self):
        global img2,im2
        filename = tk.StringVar()
        filepath = tkinter.filedialog.askopenfilename()  # 选择打开什么文件，返回文件名
        filename.set(filepath)  # 设置变量filename的值
        img2 = Image.open(filename.get())  # 打开图片
        img2 = img2.resize((200, 150), Image.ANTIALIAS)
        im2 = ImageTk.PhotoImage(img2)
        image = self.canvas.create_image(150, 220, anchor='nw', image=im2)



class RightFrm(tk.Frame):
    def __init__(self,frame):
        super(RightFrm, self).__init__(frame)
        self.canvas = tk.Canvas(self, bg='green', height=800, width=400)
        self.canvas.pack()
        # self.user1Pic = tk.Canvas(self.canvas, height=200, width=200, bg='black').place(x=50, y=50, anchor='nw')
        self.button = tk.Button(self.canvas, text='生成换脸', command=self.genImage).place(x=290, y=10, anchor='nw')
        self.button2 = tk.Button(self.canvas, text='查看完整换脸视频', command=self.create_tl).place(x=290, y=170, anchor='nw')
    def hit_me(self):
        print(tk.messagebox.askquestion(title='Hi', message='hahahaha'))

    def genImage(self):
        print(tk.messagebox.askquestion(title='Hi', message='请耐心等待'))
        time.sleep(5)
        global img3, im3
        filename = tk.StringVar()
        # filepath = tkinter.filedialog.askopenfilename()  # 选择打开什么文件，返回文件名
        filepath = '../faceswapResult/yyqx_xl_000262.png'
        print(filepath)
        filename.set(filepath)  # 设置变量filename的值
        img3 = Image.open(filename.get())  # 打开图片
        img3 = img3.resize((270,180), Image.ANTIALIAS)
        im3 = ImageTk.PhotoImage(img3)
        image = self.canvas.create_image(10, 10, anchor='nw', image=im3)


    def create_tl(self):
        toplevel = tk.Toplevel()
        toplevel.title('换脸转化视频')

        video = cv2.VideoCapture('../faceswapResult/FinalVideo.mp4')
        movieLabel = tk.Label(toplevel)
        movieLabel.pack(padx=10, pady=10)

        while video.isOpened():
            ret, frame = video.read()  # 读取照片
            # print('读取成功')
            if ret == True:
                img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)  # 转换颜色使播放时保持原有色彩
                current_image = Image.fromarray(img).resize((540, 320))  # 将图像转换成Image对象
                imgtk = ImageTk.PhotoImage(image=current_image)
                movieLabel.imgtk = imgtk
                movieLabel.config(image=imgtk)
                movieLabel.update()  # 每执行以此只显示一张图片，需要更新窗口实现视频播放

        camera.release()
        cv2.destroyAllWindonws()



class DeepfakeFrame(tk.Frame):
    def __init__(self,window, shape, locate):
        print("qhw's frame is running")
        self.label = tk.Label(window, text="faceswap Result", bg='white',padx = 5, pady = 5).\
            place(x=20, y=30, anchor='nw')
        # self.label = tk.Label(self, text="faceswap Result", padx = 5, pady = 5)
        # self.label.pack()

        # mainframe
        self.tw_window = MainFram(window,width=shape[0], height=shape[1])
        # self.tw_window.pack(padx = 0, pady = 100)
        self.tw_window.place(x=locate[0], y=locate[1])
        # leftframe -> mainframe
        self.leftFrm = LeftFrm(self.tw_window)
        self.leftFrm.pack(side = 'left')
        #
        # rightframe -> mainframe
        self.rightFrm = RightFrm(self.tw_window)
        self.rightFrm.pack(side = 'right')



