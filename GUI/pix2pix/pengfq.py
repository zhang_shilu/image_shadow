import tkinter
from tkinter import *
from tkinter import filedialog, ttk, messagebox
from PIL import Image, ImageTk
# from test import get_fake_B


class CanvasImg(object):
    def __init__(self, window, shape, locate):
        self.shape = shape
        self.canvas = Canvas(window, width=shape[0], height=shape[1])
        self.canvas.place(x=locate[0], y=locate[1])
        self.picture = None

    def show_picture(self, file_path):
        image = Image.open(file_path).resize(self.shape)
        self.picture = ImageTk.PhotoImage(image)
        self.canvas.create_image(0, 0, anchor='nw', image=self.picture)


class FileSelect(object):
    def __init__(self, window, locate, canvas):
        self.window = window
        self.locate_offset = locate
        self.btn = Button(window, text='select real A,real B', bg='DeepSkyBlue', fg='White', width=16, height=1,
                          font=('Arial', 10),
                          command=self.open_file_dialog)
        self.btn.place(x=locate[0], y=locate[1]) #locate 10 30
        self.canvas = canvas  # 显示图片的画布
        self.image_path = ''

    def open_file_dialog(self):
        file_path = filedialog.askopenfilename(title='选择图片', filetypes=[("PNG", ".png"), ("JPG", ".jpg")])
        if file_path:
            self.canvas.show_picture(file_path)
        self.image_path = file_path


class Pix2Pix(object):
    def __init__(self, window, shape, locate):
        self.window = window
        self.tw_window = Frame(window, width=shape[0], height=shape[1], bg='White') # shape 800 570
        self.tw_window.place(x=locate[0], y=locate[1])
        self.canvas1 = CanvasImg(self.tw_window, (512, 256), (locate[0]+10, locate[1] + 60))
        self.fileBtn = FileSelect(self.tw_window, (locate[0]+10, locate[1]), canvas=self.canvas1) # locate 0 30
        self.canvas2 = CanvasImg(self.tw_window, (256, 256), (locate[0]+532, locate[1]+60))
        self.runBtn = Button(self.tw_window, text='get_fake_B', bg='DeepSkyBlue', fg='White', width=8, height=1,
                             font=('Arial', 12), command=self.gen_shadow)
        self.runBtn.place(x=locate[0] + 10, y=locate[1]+460)

        real_B = tkinter.Canvas(self.tw_window, width = 100, height = 30)
        real_B.place(x = locate[0] + 78, y = locate[1]+330)
        real_B.create_rectangle(0, 0, 100, 30, fill = "lightgreen")
        real_B.create_text(50, 15, text = "real_B")

        real_A = tkinter.Canvas(self.tw_window, width = 100, height = 30)
        real_A.place(x = locate[0] + 334, y = locate[1] + 330)
        real_A.create_rectangle(0, 0, 100, 30, fill = "lightgreen")
        real_A.create_text(50, 15, text = "real_A")

        fake_B = tkinter.Canvas(self.tw_window, width = 100, height = 30)
        fake_B.place(x = locate[0] + 590, y = locate[1] + 330)
        fake_B.create_rectangle(0, 0, 100, 30, fill = "lightgreen")
        fake_B.create_text(50, 15, text = "fake_B")

    def gen_shadow(self):
        if self.fileBtn.image_path:
            new_path = get_fake_B(self.fileBtn.image_path)
            self.canvas2.show_picture(new_path)
        else:
            messagebox.showerror('错误', '必须选择图片', parent=self.window)
