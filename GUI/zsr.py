import tkinter
from tkinter import *
from tkinter import filedialog, ttk, messagebox
from PIL import Image, ImageTk
from predict import predict


class CanvasImg(object):
    def __init__(self, window, shape, locate):
        self.shape = shape
        self.canvas = Canvas(window, width=shape[0], height=shape[1])
        self.canvas.place(x=locate[0], y=locate[1])
        self.picture = None

    def show_picture(self, file_path):
        image = Image.open(file_path).resize(self.shape)
        self.picture = ImageTk.PhotoImage(image)
        self.canvas.create_image(0, 0, anchor='nw', image=self.picture)


class FileSelect(object):
    def __init__(self, window, locate, canvas):
        self.window = window
        self.locate_offset = locate
        self.btn = Button(window, text='选择图片', bg='DeepSkyBlue', fg='White', width=8, height=1,
                          font=('Arial', 12),
                          command=self.open_file_dialog)
        self.btn.place(x=locate[0], y=locate[1]) #locate 10 30
        self.canvas = canvas  # 显示图片的画布
        self.image_path = ''

    def open_file_dialog(self):
        file_path = filedialog.askopenfilename(title='选择图片', filetypes=[("PNG", ".png"), ("JPG", ".jpg")])
        if file_path:
            self.canvas.show_picture(file_path)
        self.image_path = file_path


class Downselect(object):
    def __init__(self, window, locate):
        self.down_select = ttk.Combobox(window, width=22)
        self.down_select.place(x=locate[0], y=locate[1])
        self.light_direction = None
        self.value = ('rear center', 'front center', 'center top, front lighting',
                                     'center top, side lighting', 'center top, back lighting',
                                     'upper right, front lighting', 'upper right, side lighting',
                                     'upper right, back lighting', 'center right, front lighting',
                                     'center right, side lighting', 'center right, back lighting',
                                     'lower right, front lighting', 'lower right, side lighting',
                                     'lower right, back lighting', 'bottom, front lighting',
                                     'bottom, side lighting', 'bottom, back lighting',
                                     'lower left, front lighting', 'lower left, side lighting',
                                     'lower left, back lighting', 'center left, front lighting',
                                     'center left, side lighting', 'center left, back lighting',
                                     'upper left, front lighting', 'upper left, side lighting',
                                     'upper left, back lighting')
        self.down_select['value'] = self.value
        self.light_direction_num = ['001', '002', '110', '120', '130', '210', '220', '230',
                                    '310', '320', '330', '410', '420', '430',
                                    '510', '520', '530', '610', '620', '630',
                                    '710', '720', '730', '810', '820', '830']
        self.down_select.bind("<<ComboboxSelected>>", self.selected)

    def selected(self, event):
        self.light_direction = self.light_direction_num[self.value.index(self.down_select.get())]

class ShadowSketchFrame(object):
    def __init__(self, window, shape, locate):
        self.window = window
        self.tw_window = Frame(window, width=shape[0], height=shape[1], bg='White') # shape 800 570
        self.tw_window.place(x=locate[0], y=locate[1])
        self.canvas1 = CanvasImg(self.tw_window, (380, 380), (locate[0]+10, locate[1]+60))
        self.fileBtn = FileSelect(self.tw_window, (locate[0]+10, locate[1]), canvas=self.canvas1) # locate 0 30
        self.canvas2 = CanvasImg(self.tw_window, (380, 380), (locate[0]+405, locate[1]+60))
        self.runBtn = Button(self.tw_window, text='生成', bg='DeepSkyBlue', fg='White', width=8, height=1,
                             font=('Arial', 12), command=self.gen_shadow)
        self.runBtn.place(x=locate[0]+300, y=locate[1]+460)

        self.select_label = Label(window, text="光照方向:", font=('Arial', 12), bg='White')
        self.select_label.place(x=10, y=520)
        self.combobox = Downselect(window, (100, 520))

    def gen_shadow(self):
        if self.combobox.light_direction and self.fileBtn.image_path:
            new_path = predict(self.fileBtn.image_path, self.combobox.light_direction)
            self.canvas2.show_picture(new_path)
        else:
            messagebox.showerror('错误', '必须选择图片和光照方向', parent=self.window)
