# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:08:38 2020

@author: tianjie 
"""
from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk

import sys
import os

sys.path.append('..\\')

from .functions import *


image_file = None

class ImgLabel(object):
    def __init__(self, window, shape, locate):
        self.shape = shape
        self.image_label = Label(window, width=shape[0], height=shape[1])
        self.image_label.place(x=locate[0], y=locate[1])
        self.image_path=None

    def show_picture(self, file_path):
        global image_file
        self.image_path=file_path
        img = Image.open(file_path).resize(self.shape)
        image_file = ImageTk.PhotoImage(img)
        # print(image_file)
        self.image_label.configure(image = image_file)
        self.image_label.image = image_file # keep a reference!
        

def open_file_dialog(img_label):
        file_path = filedialog.askopenfilename(title='select color', filetypes=[("PNG", ".png"), ("JPG", ".jpg")])
        if file_path:
            img_label.show_picture(file_path)
            

class DeepNormalFrame(object):
    def __init__(self, window, shape, locate):
        self.tw_window = Frame(window, width=shape[0], height=shape[1]) # shape 800 570
        self.tw_window.place(x=0, y=30)
        
        self.ImgLabel1= ImgLabel(self.tw_window, (180, 180), (locate[0]+45, locate[1]+30))
        self.fileBtn1 = Button(self.tw_window, text='select line_image', bg='DeepSkyBlue', fg='White', width=15, height=1,
                          font=('Arial', 9),
                          command=lambda:open_file_dialog(self.ImgLabel1))
        self.fileBtn1.place(x=locate[0]+80, y=locate[1])    
        
        
        
        self.ImgLabel2 =ImgLabel(self.tw_window, (180, 180), (locate[0]+316, locate[1]+30))
        self.fileBtn2 = Button(self.tw_window, text='select mask_image', bg='DeepSkyBlue', fg='White', width=15, height=1,
                          font=('Arial', 9),
                          command=lambda:open_file_dialog(self.ImgLabel2))
        self.fileBtn2.place(x=locate[0]+350, y=locate[1])
        
        
        self.ImgLabel3= ImgLabel(self.tw_window, (180, 180), (locate[0]+564, locate[1]+30))
        self.fileBtn3 = Button(self.tw_window, text='select color_image', bg='DeepSkyBlue', fg='White', width=15, height=1,
                          font=('Arial', 9),
                          command=lambda:open_file_dialog(self.ImgLabel3))
        self.fileBtn3.place(x=locate[0]+600, y=locate[1])
    
        
        label=Label(self.tw_window,width=800,height=1,bg="green")
        label.place(x=0, y=locate[1]+220)
        
        self.ImgLabel4= ImgLabel(self.tw_window, (200, 200), (170, 320))
        self.runBtn1 = Button(self.tw_window, text='normal estimate', bg='DeepSkyBlue', fg='White', width=15, height=1,
                             font=('Arial', 9),
                              command=lambda:normalEstimate(self.ImgLabel4,self.ImgLabel1.image_path,self.ImgLabel2.image_path))
        self.runBtn1.place(x=locate[0]+220, y=locate[1]+250)
        
        self.ImgLabel5= ImgLabel(self.tw_window, (200, 200), (480, 320))
        self.runBtn2 = Button(self.tw_window, text='rendering', bg='DeepSkyBlue', fg='White', width=15, height=1,
                             font=('Arial', 9),
                             command=lambda:Rendering(self.ImgLabel5,self.ImgLabel1.image_path,self.ImgLabel2.image_path,self.ImgLabel3.image_path))
        self.runBtn2.place(x=locate[0]+530, y=locate[1]+250)
