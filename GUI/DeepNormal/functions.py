# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 11:37:15 2020

@author: Administrator
"""
import cv2
import os
from tqdm import tqdm
import numpy as np 
import math
import sys
import argparse

import tensorflow as tf
import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
from tflearn.layers.normalization import l2_normalize
from tflearn.metrics import Top_k, R2

sys.path.append('..\\')


from .tianjie import *
from .model import GenerateNet

import sys
print(sys.path)

img = None
   
imgN = None
   
Mask = None

color = None
  

    
"""
some functions

"""
def load_linedrawing(Path):
	#print('loading' + Path)
	img = cv2.imread(Path, cv2.IMREAD_GRAYSCALE)
	img = cv2.bitwise_not(img) #invert image
	ret,thresh1 = cv2.threshold(img,24,255,cv2.THRESH_BINARY)
	return thresh1

def load_image(Path):
	print('loading' + Path)
	img = cv2.imread(Path, cv2.IMREAD_GRAYSCALE)
	#img = cv2.bitwise_not(img) #invert image not for rendering
	return img

def load_mask(Path):
	#print('loading' + Path)
	mask = cv2.imread(Path, cv2.IMREAD_GRAYSCALE)
	return mask

def load_color(Path):
	#print('loading' + Path)
	return cv2.imread(Path, cv2.IMREAD_UNCHANGED)

def load_normal(Path):
	print('loading' + Path)
	imgN = cv2.imread(Path)
	return imgN

def neighbours_vec(image):
    return image[2:,1:-1], image[2:,2:], image[1:-1,2:], image[:-2,2:], image[:-2,1:-1],     image[:-2,:-2], image[1:-1,:-2], image[2:,:-2]

def transitions_vec(P2, P3, P4, P5, P6, P7, P8, P9):
    return ((P3-P2) > 0).astype(int) + ((P4-P3) > 0).astype(int) + \
    ((P5-P4) > 0).astype(int) + ((P6-P5) > 0).astype(int) + \
    ((P7-P6) > 0).astype(int) + ((P8-P7) > 0).astype(int) + \
    ((P9-P8) > 0).astype(int) + ((P2-P9) > 0).astype(int)

def zhangSuen_vec(image, iterations):
    for iter in tqdm(range (1, iterations)):
        #print(iter)
        # step 1    
        P2,P3,P4,P5,P6,P7,P8,P9 = neighbours_vec(image)
        condition0 = image[1:-1,1:-1]
        condition4 = P4*P6*P8
        condition3 = P2*P4*P6
        condition2 = transitions_vec(P2, P3, P4, P5, P6, P7, P8, P9) == 1
        condition1 = (2 <= P2+P3+P4+P5+P6+P7+P8+P9) * (P2+P3+P4+P5+P6+P7+P8+P9 <= 6)
        cond = (condition0 == 1) * (condition4 == 0) * (condition3 == 0) * (condition2 == 1) * (condition1 == 1)
        changing1 = np.where(cond == 1)
        image[changing1[0]+1,changing1[1]+1] = 0
        # step 2
        P2,P3,P4,P5,P6,P7,P8,P9 = neighbours_vec(image)
        condition0 = image[1:-1,1:-1]
        condition4 = P2*P6*P8
        condition3 = P2*P4*P8
        condition2 = transitions_vec(P2, P3, P4, P5, P6, P7, P8, P9) == 1
        condition1 = (2 <= P2+P3+P4+P5+P6+P7+P8+P9) * (P2+P3+P4+P5+P6+P7+P8+P9 <= 6)
        cond = (condition0 == 1) * (condition4 == 0) * (condition3 == 0) * (condition2 == 1) * (condition1 == 1)
        changing2 = np.where(cond == 1)
        image[changing2[0]+1,changing2[1]+1] = 0
    return 255*image

def BorderHandle(x, size_2, lenn):
	if (x - size_2) < 0:
		xm = 0
		Xm = size_2 - x
	else:
		xm = (x - size_2)
		Xm = 0

	if(x + size_2) > lenn:
		xM = lenn
		XM = size_2 + (lenn - x)
	else:
		xM = x + size_2
		XM = 2*size_2

	return (xm, xM, Xm, XM)


def CropMultiScale_ZeroPadding_2(x, y, image, image_2, image_4, size):
	img_blank = np.zeros((size, size, 3), np.float32)
	x1 = int(x / 2) + size + 1
	y1 = int(y / 2)	+ size + 1
	x2 = int(x / 4) + size + 1
	y2 = int(y / 4) + size + 1
	x = x + size + 1
	y = y + size + 1
	size = int(size / 2)

	xm, xM, Xm, XM = BorderHandle(x1, size, image_2.shape[1])
	ym, yM, Ym, YM = BorderHandle(y1, size, image_2.shape[0])
	img_blank[Ym:YM,Xm:XM,1] = image_2[ym:yM, xm:xM]

	xm, xM, Xm, XM = BorderHandle(x2, size, image_4.shape[1])
	ym, yM, Ym, YM = BorderHandle(y2, size, image_4.shape[0])
	img_blank[Ym:YM,Xm:XM,2] = image_4[ym:yM, xm:xM]

	xm, xM, Xm, XM = BorderHandle(x, size, image.shape[1])
	ym, yM, Ym, YM = BorderHandle(y, size, image.shape[0])
	img_blank[Ym:YM,Xm:XM,0] = image[ym:yM, xm:xM]

	img_blank = img_blank / 127.5 - 1.0
	return img_blank

def PrepareMultiScale(img):
	size = 256
	img_pad = np.zeros((img.shape[0] + 2 * size, img.shape[1] + 2 * size), np.float32)
	img_pad[size + 1:(img.shape[0]+size + 1), size + 1:(img.shape[1]+size + 1)] = img
	
	#resized version of image for global view
	img_2tmp = cv2.resize(img, None, fx=0.5, fy=0.5, interpolation= cv2.INTER_LINEAR)
	img_2 = np.zeros((img_2tmp.shape[0] + 2 * size, img_2tmp.shape[1] + 2 * size), np.float32)
	img_2[size + 1:(img_2tmp.shape[0]+size + 1), size + 1:(img_2tmp.shape[1]+size + 1)] = img_2tmp
	
	img_4tmp = cv2.resize(img_2tmp, None, fx=0.5, fy=0.5, interpolation= cv2.INTER_LINEAR)
	img_4 = np.zeros((img_4tmp.shape[0] + 2 * size, img_4tmp.shape[1] + 2 * size), np.float32)
	img_4[size + 1:(img_4tmp.shape[0]+size + 1), size + 1:(img_4tmp.shape[1]+size + 1)] = img_4tmp

	return img_pad, img_2, img_4

def MaskToInput(img, Mask):
	NonzeroPointsMask1, NonzeroPointsMask2 = np.nonzero(Mask)
	#print(np.shape(NonzeroPointsMask2))
	Non= np.array((NonzeroPointsMask1, NonzeroPointsMask2))
	for l in range(len(NonzeroPointsMask2)):
		x = Non[1][l]
		y = Non[0][l]
		if(img[y, x] != 255.0):
			img[y, x] = 160
	return img

def CleanWithMask(img, Mask):
	height, width = Mask.shape
	t1 = np.stack((Mask, Mask, Mask), axis = 2)
	t1 = t1 / 255.0
	t2 = 1.0 - t1
	img[0:height, 0:width, :] = img[0:height, 0:width, :] * t1 + t2* 255.0
	return img

def Normalize(x):
	Norm = np.sqrt(x[0]*x[0] + x[1]*x[1] +x[2]*x[2])
	if Norm == 0.0:
		return x
	return x / Norm


"""
rendering

"""

#Some function definition for image based rendering
def GuidedFiltF(img, r):
    eps = 0.04;
    I = img
    I2 = cv2.pow(I,2);
    mean_I = cv2.boxFilter(I,-1,((2*r)+1,(2*r)+1))
    mean_I2 = cv2.boxFilter(I2,-1,((2*r)+1,(2*r)+1))
    
    cov_I = mean_I2 - cv2.pow(mean_I,2);
    
    var_I = cov_I;
    
    a = cv2.divide(cov_I,var_I+eps)
    b = mean_I - (a*mean_I)
    
    mean_a = cv2.boxFilter(a,-1,((2*r)+1,(2*r)+1))
    mean_b = cv2.boxFilter(b,-1,((2*r)+1,(2*r)+1))
    
    q = (mean_a * I) + mean_b;
    return(q)

def ComputeLightDirectionMat(Xpos, Ypos, Zpos, IndexMat3D):
	out = np.copy(IndexMat3D)
	Z = IndexMat3D[:,:,0] + Zpos
	Y = IndexMat3D[:,:,1] - Ypos
	X = Xpos - IndexMat3D[:,:,2]

	SUM = np.sqrt(X**2 + Y**2 + Z**2)

	out[:,:,0] = Z / SUM
	out[:,:,1] = Y / SUM
	out[:,:,2] = X / SUM

	return out


def CreateIndexMat(height, width):
	ind = np.zeros((height, width, 3))
	for j in range(0, height):
		for i in range (0, width):
			ind[j,i,0] = 0
			ind[j,i,1] = j
			ind[j,i,2] = i
	return ind

def ComputeFresnel(dot, ior):
	height, width = dot.shape
	cosi = np.copy(dot)
	etai = np.ones((height, width))
	#etat = np.ones((height, width)) * ior
	etat = ior
	# Snell's law
	sint = etai/etat * np.sqrt(np.maximum(0.0, cosi * cosi))
	#Total Reflection
	sint2= np.copy(sint)
	#sint[np.where(sint >=1)] = 1

	cost = np.sqrt(np.maximum(0.0, 1 - sint * sint))
	cosi = abs(cosi)
	sint = (((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost))**2 + ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost))**2)/2.0
	sint[np.where(sint2 >=1)] = 1
	#Rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost))
	#Rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost))
	#kr = (Rs * Rs + Rp * Rp) / 2

	return  1-sint

def Rendering(imgLabel,line_image,mask_image,color_image):
    parser = argparse.ArgumentParser(description='')
    
    parser.add_argument('--normal_path', dest='normal_path', default='DeepNormal/results/Normal_Map.png', help='Path of the normal map')
    parser.add_argument('--r', dest='r', type= float, default=0.99, help='r value for light')
    parser.add_argument('--g', dest='g', type= float, default=0.83, help='r value for light')
    parser.add_argument('--b', dest='b', type= float, default=0.66, help='r value for light')
    parser.add_argument('--save_path', dest ='save_path', default='DeepNormal/results/', help='Path of the save folder')
    args = parser.parse_args()
    
    
    
    Light_direction = Normalize([0,0,1]) #update realtime later
    
    
    img = load_linedrawing(line_image)
    #Load Mask
    Mask = load_mask(mask_image)
    
    #Load Normal Map
    imgN = load_normal(args.normal_path)
    
    
    #Load Color
    color = load_color(color_image)
    
    print(" ")
    
    height, width = img.shape
    
    imgN = imgN / 127.5 - 1.0
    
    #Some Init
    pi = math.pi
    threshold = 100
    Xpos = 0
    Ypos = 0
    Zpos = 100
    filtering = 0
    amb = 0.55
    ks = 0
    alpha =10
    num = 0
    ind = CreateIndexMat(height, width)
    r = args.r
    g = args.g
    b = args.b
    Plight = 0.8
    
    if(filtering >0):
    	imgN2 = GuidedFiltF(imgN, filtering)
    else :
    	imgN2 = np.copy(imgN)
    
    LD = ComputeLightDirectionMat(Xpos, Ypos, Zpos, ind)
    
    dot = np.sum(imgN2 * LD, axis = 2)
    
    dot[np.where(dot<0)]=0
    dot[np.where(dot>1.0)]=1.0
    
    dot3 = np.stack((dot,dot,dot), axis = 2)
    R = (np.multiply(2*dot3,imgN2) - LD)[:,:,0]
    R[np.where(R<0)]=0
    
    
    Rspec = (R**alpha)
    RspecR = (R**(50.0 * alpha/10.0))
    RspecG = (R**(50.0 * alpha/10.0))
    RspecB = (R**(53.47* alpha/10.0))
    
    #Schlik
    FresnelR = RspecR + (1-RspecR) * (1.0-R)**5
    FresnelG = RspecG + (1-RspecG) * (1.0-R)**5
    FresnelB = RspecB + (1-RspecB) * (1.0-R)**5
    
    dstImage = dot
    dot8 = (dot*255).astype(np.dtype('uint8'))
    color64 = color.astype(np.dtype('float64'))
    
    color64[:,:,0] = np.minimum(255.0, color64[:,:,0] * amb * b + Plight * color64[:,:,0] * dstImage * b + Plight * b * 1.58*ks * RspecB*FresnelB)
    color64[:,:,1] = np.minimum(255.0, color64[:,:,1] * amb * g + Plight * color64[:,:,1] * dstImage * g + Plight * g * 1.50*ks * RspecG*FresnelG)
    color64[:,:,2] = np.minimum(255.0, color64[:,:,2] * amb * r + Plight * color64[:,:,2] * dstImage * r + Plight * r * 1.35*ks * RspecR*FresnelR)
    
    color64[np.where(Mask == 0)]= 255
    final = color64.astype(np.dtype('uint8'))    
    cv2.imwrite(args.save_path+'final.png', final)
    imgLabel.show_picture('DeepNormal/results/final.png')
    


"""
normal estimate

"""

def normalEstimate(imgLabel,line_image,mask_image):
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--save_path', dest ='save_path', default='DeepNormal/results/', help='Path of the save folder')
    parser.add_argument('--model_name', dest='model_name', default='DeepNormal/pretrained_model/DeepNormals', help='Path of the Model')
    parser.add_argument('--thining_it', dest='thining_it', type=int, default=10, help='Number of iterations for ZhangSuen thining algorithm')
    parser.add_argument('--nb_grids', dest='nb_grids', type=int, default=40, help='Number of tile grids for reconstruction')
    args = parser.parse_args()
    
    
    
    print(line_image)
    print(mask_image)
    #First prepare input data
    img = load_linedrawing(line_image)
    #Load Mask
    Mask = load_mask(mask_image)
    
    #Thining algorithm
    print('Thining Input image: ')
    img = zhangSuen_vec(img/255.0, args.thining_it)
    
    #print("Apply mask correction to image") *! Different from Paper (minor change)!*
    img = MaskToInput(img, Mask)
    
    #Padding and preparing multiscale images
    img_pad, img_2, img_4 = PrepareMultiScale(img)
    
    height, width = img.shape
    size= 256
    tf.reset_default_graph()
    model=None
    MODEL_NAME=None
    #Generate and Load model
    model = GenerateNet()
    #Load Network
  
    MODEL_NAME = args.model_name

    with tf.Session() as sess:
    	if os.path.exists('{}.meta'.format(MODEL_NAME)):
    		print('model: ' + MODEL_NAME + ' loading!')
    		model.load(MODEL_NAME)
    		print('model: ' + MODEL_NAME + ' loaded!')
    	else:
    		sys.exit('Error: ' + MODEL_NAME + ' Not Found')


    ind =0.0
    
    recfin = np.zeros((height + 600, width + 600, 3)).astype(float)
    recTrim = []
    print('Predicting grids:')
    for offset in tqdm(range(0, 256, int(256/args.nb_grids))):
    	SubBatch = []
    	Pos = []
    	index = 0.0
    
    	for j in range(int(height / 256) + 2):
    		y = j * 256 + offset -128
    		for i in range(int(width / 256) + 2):
    			x = i* 256 + offset -128
    			#st = time.time()
    			Sub = CropMultiScale_ZeroPadding_2(x, y, img_pad, img_2, img_4, size)
    			#Sub[Sub<0.3]=0
    			#et = time.time()-st
    			#print("Cropp: " + str(et))
    			#cv2.imshow('Sub', Sub)
    			#cv2.waitKey(0)
    			SubBatch.append(Sub)
    			index = index + 1.0
    			Pos.append([x,y])
    
    	S = np.array(SubBatch)
    	predN = model.predict({'input' : S})
    	rec = np.zeros((height + 900, width + 900, 3)).astype(float)
    	off = 260
    	s = int(size/2)
    	ind += 1.0
    	for i in range(int(index)):
    		x = off + Pos[i][0]
    		y = off + Pos[i][1]
    		rec[(y-s):(y+s), (x-s):(x+s),:] += predN[i]
    	recfin[0: height, 0:width] += rec[260: height + 260, 260:width+260]
    
    
    
    #Average Tiles
    recfin = (recfin/(ind)) *127.5 + 127.5
    
    #Clean result
    recfin = CleanWithMask(recfin, Mask)
    
    #Remove Padding
    final = np.zeros((height, width,3))
    final = recfin[0:height, 0:width, :]
    
   
    #Write result
    cv2.imwrite(args.save_path +'Normal_Map.png', final)
    print('result normal map saved in: ' +args.save_path +'Normal_Map.png') 
    imgLabel.show_picture('DeepNormal/results/Normal_Map.png')
    
    model=None
    MODEL_NAME=None