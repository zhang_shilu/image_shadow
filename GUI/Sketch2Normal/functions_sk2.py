'''
Sketch2Normal implementation using Tensorflow for paper
Interactive Sketch-Based Normal Map Generation with Deep Neural Networks
Author Wanchao Su
This code is based on the implementation of pix2pix from  https://github.com/yenchenlin/pix2pix-tensorflow
'''
import os
import time
import random
import numpy as np
from glob import glob
import scipy.misc

import tensorflow  as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()

# tf.disable_v2_behavior()
import os
import argparse


class batch_norm(object):

    def __init__(self, epsilon=1e-5, momentum=0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, train=True):
        return tf.contrib.layers.batch_norm(x, decay=self.momentum, updates_collections=None, epsilon=self.epsilon,
                                            scale=True, scope=self.name)


def conv2d(input_, output_dim,
           k_h=5, k_w=5, d_h=2, d_w=2, stddev=0.02,
           name="conv2d"):
    with tf.variable_scope(name):
        w = tf.get_variable('w', [k_h, k_w, input_.get_shape()[-1], output_dim],
                            initializer=tf.truncated_normal_initializer(stddev=stddev))
        conv = tf.nn.conv2d(input_, w, strides=[1, d_h, d_w, 1], padding='SAME')

        biases = tf.get_variable('biases', [output_dim], initializer=tf.constant_initializer(0.0))
        conv = tf.reshape(tf.nn.bias_add(conv, biases), conv.get_shape())

        return conv


def deconv2d(input_, output_shape,
             k_h=5, k_w=5, d_h=2, d_w=2, stddev=0.02,
             name="deconv2d", with_w=False):
    with tf.variable_scope(name):
        # filter : [height, width, output_channels, in_channels]
        w = tf.get_variable('w', [k_h, k_w, output_shape[-1], input_.get_shape()[-1]],
                            initializer=tf.random_normal_initializer(stddev=stddev))

        try:
            deconv = tf.nn.conv2d_transpose(input_, w, output_shape=output_shape,
                                            strides=[1, d_h, d_w, 1])

        # Support for verisons of TensorFlow before 0.7.0
        except AttributeError:
            deconv = tf.nn.deconv2d(input_, w, output_shape=output_shape,
                                    strides=[1, d_h, d_w, 1])

        biases = tf.get_variable('biases', [output_shape[-1]], initializer=tf.constant_initializer(0.0))
        deconv = tf.reshape(tf.nn.bias_add(deconv, biases), deconv.get_shape())

        if with_w:
            return deconv, w, biases
        else:
            return deconv


def lrelu(x, leak=0.2, name="lrelu"):
    return tf.maximum(x, leak * x)


def linear(input_, output_size, scope=None, stddev=0.02, bias_start=0.0, with_w=False):
    shape = input_.get_shape().as_list()

    with tf.variable_scope(scope or "Linear"):
        matrix = tf.get_variable("Matrix", [shape[1], output_size], tf.float32,
                                 tf.random_normal_initializer(stddev=stddev))
        bias = tf.get_variable("bias", [output_size],
                               initializer=tf.constant_initializer(bias_start))
        if with_w:
            return tf.matmul(input_, matrix) + bias, matrix, bias
        else:
            return tf.matmul(input_, matrix) + bias


class normalnet(object):
    def __init__(self, sess, image_size=256,
                 batch_size=1, sample_size=1, output_size=256,
                 gf_dim=64, df_dim=64, L1_lambda=100, n_critic=5, clamp=0.01,
                 input_c_dim=3, output_c_dim=3, dataset_name='primitive', coefficient=100):

        self.sess = sess
        self.clamp = clamp
        self.batch_size = batch_size
        self.image_size = image_size
        self.sample_size = sample_size
        self.output_size = output_size

        self.gf_dim = gf_dim
        self.df_dim = df_dim

        self.input_c_dim = input_c_dim
        self.output_c_dim = output_c_dim

        self.L1_lambda = L1_lambda
        self.coefficient = coefficient
        self.n_critic = n_critic

        self.d_bn1 = batch_norm(name='d_bn1')
        self.d_bn2 = batch_norm(name='d_bn2')
        self.d_bn3 = batch_norm(name='d_bn3')

        self.g_bn_e2 = batch_norm(name='g_bn_e2')
        self.g_bn_e3 = batch_norm(name='g_bn_e3')
        self.g_bn_e4 = batch_norm(name='g_bn_e4')
        self.g_bn_e5 = batch_norm(name='g_bn_e5')
        self.g_bn_e6 = batch_norm(name='g_bn_e6')
        self.g_bn_e7 = batch_norm(name='g_bn_e7')
        self.g_bn_e8 = batch_norm(name='g_bn_e8')

        self.g_bn_d1 = batch_norm(name='g_bn_d1')
        self.g_bn_d2 = batch_norm(name='g_bn_d2')
        self.g_bn_d3 = batch_norm(name='g_bn_d3')
        self.g_bn_d4 = batch_norm(name='g_bn_d4')
        self.g_bn_d5 = batch_norm(name='g_bn_d5')
        self.g_bn_d6 = batch_norm(name='g_bn_d6')
        self.g_bn_d7 = batch_norm(name='g_bn_d7')

        self.dataset_name = dataset_name
        self.build_model()

    def build_model(self):
        self.real_data = tf.placeholder(tf.float32,
                                        [self.batch_size, self.image_size, self.image_size,
                                         self.input_c_dim + self.output_c_dim + 1],
                                        name='real_A_and_B_images')

        self.real_A = self.real_data[:, :, :, :self.input_c_dim]
        self.real_B = self.real_data[:, :, :, self.input_c_dim + 1:self.input_c_dim + self.output_c_dim + 1]
        self.mask = self.real_data[:, :, :, self.input_c_dim:self.input_c_dim + 1]

        fake_B = self.generator(tf.concat([self.real_A, self.mask], axis=3))
        mask = tf.concat([self.mask, self.mask, self.mask], axis=3)
        self.fake_B = tf.add(tf.multiply(1 - mask, fake_B), tf.multiply(mask, self.real_A))

        self.real_AB = tf.concat([self.real_A, self.real_B], 3)
        self.fake_AB = tf.concat([self.real_A, self.fake_B], 3)
        self.D_logits = self.discriminator(self.real_AB, reuse=False)
        self.D_logits_ = self.discriminator(self.fake_AB, reuse=True)

        self.fake_B_sample = self.generator(tf.concat([self.real_A, self.mask], axis=3), isSampling=True)
        self.fake_B_sum = tf.summary.image("fake_B", self.fake_B)
        self.real_B_sum = tf.summary.image("real_B", self.real_B)

        self.d_loss_real = tf.reduce_mean(self.D_logits)
        self.d_loss_fake = tf.reduce_mean(self.D_logits_)

        filter = 1.0 / 273 * tf.constant([[1, 4, 7, 4, 1],
                                          [4, 16, 26, 16, 4],
                                          [7, 26, 41, 26, 7],
                                          [4, 16, 26, 16, 4],
                                          [1, 4, 7, 4, 1]], tf.float32)

        filter = tf.reshape(filter, [5, 5, 1, 1])
        self.masked_loss = tf.reduce_mean(
            tf.multiply(tf.nn.conv2d(self.mask, filter, strides=[1, 1, 1, 1], padding='SAME'),
                        tf.reduce_sum(tf.abs(self.real_B - self.fake_B), axis=3)))
        self.pixel_wised_loss = tf.reduce_mean(tf.abs(self.real_B - self.fake_B))

        self.d_loss = self.d_loss_fake - self.d_loss_real
        self.g_loss = - self.d_loss_fake \
                      + self.L1_lambda * self.pixel_wised_loss \
                      + self.coefficient * self.L1_lambda * self.masked_loss

        self.d_loss_real_sum = tf.summary.scalar("d_loss_real", self.d_loss_real)
        self.d_loss_fake_sum = tf.summary.scalar("d_loss_fake", self.d_loss_fake)

        self.g_loss_sum = tf.summary.scalar("g_loss", self.g_loss)
        self.d_loss_sum = tf.summary.scalar("d_loss", self.d_loss)

        self.masked_loss_sum = tf.summary.scalar("masked_loss", self.masked_loss)
        self.pixel_wised_loss_sum = tf.summary.scalar("pixeled_loss", self.pixel_wised_loss)

        t_vars = tf.trainable_variables()

        self.d_vars = [var for var in t_vars if 'd_' in var.name]
        self.g_vars = [var for var in t_vars if 'g_' in var.name]

        self.saver = tf.train.Saver()

    # def load_random_samples(self):
    #     data = np.random.choice(glob('./datasets/{}/val/*.png'.format(self.dataset_name)), self.batch_size)
    #     sample = [self.load_data(sample_file) for sample_file in data]
    #     sample_images = np.array(sample).astype(np.float32)
    #     return sample_images

    # def sample_model(self, sample_dir, epoch, idx):
    #     sample_images = self.load_random_samples()
    #     samples, d_loss, g_loss = self.sess.run(
    #         [self.fake_B_sample, self.d_loss, self.g_loss],
    #         feed_dict={self.real_data: sample_images}
    #     )
    #     self.save_images(samples, [self.batch_size, 1], './{}/train_{:02d}_{:04d}.png'.format(sample_dir, epoch, idx))
    #     print("[Sample] d_loss: {:.8f}, g_loss: {:.8f}".format(d_loss, g_loss))

    def discriminator(self, image, y=None, reuse=False):

        with tf.variable_scope("discriminator") as scope:

            if reuse:
                tf.get_variable_scope().reuse_variables()
            else:
                assert tf.get_variable_scope().reuse == False

            h0 = lrelu(conv2d(image, self.df_dim, name='d_h0_conv'))
            h1 = lrelu(self.d_bn1(conv2d(h0, self.df_dim * 2, name='d_h1_conv')))
            h2 = lrelu(self.d_bn2(conv2d(h1, self.df_dim * 4, name='d_h2_conv')))
            h3 = lrelu(self.d_bn3(conv2d(h2, self.df_dim * 8, d_h=1, d_w=1, name='d_h3_conv')))
            h4 = linear(tf.reshape(h3, [self.batch_size, -1]), 1, 'd_h3_lin')

            return h4

    def generator(self, image, isSampling=False):
        with tf.variable_scope("generator") as scope:
            if isSampling:
                scope.reuse_variables()

            s = self.output_size
            s2, s4, s8, s16, s32, s64, s128 = int(s / 2), int(s / 4), int(s / 8), int(s / 16), int(s / 32), int(
                s / 64), int(s / 128)

            # image is (256 x 256 x 4)
            e1 = conv2d(image, self.gf_dim, name='g_e1_conv')
            e2 = self.g_bn_e2(conv2d(lrelu(e1), self.gf_dim * 2, name='g_e2_conv'))
            e3 = self.g_bn_e3(conv2d(lrelu(e2), self.gf_dim * 4, name='g_e3_conv'))
            e4 = self.g_bn_e4(conv2d(lrelu(e3), self.gf_dim * 8, name='g_e4_conv'))
            e5 = self.g_bn_e5(conv2d(lrelu(e4), self.gf_dim * 8, name='g_e5_conv'))
            e6 = self.g_bn_e6(conv2d(lrelu(e5), self.gf_dim * 8, name='g_e6_conv'))
            e7 = self.g_bn_e7(conv2d(lrelu(e6), self.gf_dim * 8, name='g_e7_conv'))
            e8 = self.g_bn_e8(conv2d(lrelu(e7), self.gf_dim * 8, name='g_e8_conv'))

            self.d1, self.d1_w, self.d1_b = deconv2d(tf.nn.relu(e8),
                                                     [self.batch_size, s128, s128, self.gf_dim * 8], name='g_d1',
                                                     with_w=True)
            d1 = tf.nn.dropout(self.g_bn_d1(self.d1), 0.5)
            d1 = tf.concat([d1, e7], 3)

            self.d2, self.d2_w, self.d2_b = deconv2d(tf.nn.relu(d1),
                                                     [self.batch_size, s64, s64, self.gf_dim * 8], name='g_d2',
                                                     with_w=True)
            d2 = tf.nn.dropout(self.g_bn_d2(self.d2), 0.5)
            d2 = tf.concat([d2, e6], 3)

            self.d3, self.d3_w, self.d3_b = deconv2d(tf.nn.relu(d2),
                                                     [self.batch_size, s32, s32, self.gf_dim * 8], name='g_d3',
                                                     with_w=True)
            d3 = tf.nn.dropout(self.g_bn_d3(self.d3), 0.5)
            d3 = tf.concat([d3, e5], 3)

            self.d4, self.d4_w, self.d4_b = deconv2d(tf.nn.relu(d3),
                                                     [self.batch_size, s16, s16, self.gf_dim * 8], name='g_d4',
                                                     with_w=True)
            d4 = self.g_bn_d4(self.d4)
            d4 = tf.concat([d4, e4], 3)

            self.d5, self.d5_w, self.d5_b = deconv2d(tf.nn.relu(d4),
                                                     [self.batch_size, s8, s8, self.gf_dim * 4], name='g_d5',
                                                     with_w=True)
            d5 = self.g_bn_d5(self.d5)
            d5 = tf.concat([d5, e3], 3)

            self.d6, self.d6_w, self.d6_b = deconv2d(tf.nn.relu(d5),
                                                     [self.batch_size, s4, s4, self.gf_dim * 2], name='g_d6',
                                                     with_w=True)
            d6 = self.g_bn_d6(self.d6)
            d6 = tf.concat([d6, e2], 3)

            self.d7, self.d7_w, self.d7_b = deconv2d(tf.nn.relu(d6),
                                                     [self.batch_size, s2, s2, self.gf_dim], name='g_d7', with_w=True)
            d7 = self.g_bn_d7(self.d7)
            d7 = tf.concat([d7, e1], 3)

            self.d8, self.d8_w, self.d8_b = deconv2d(tf.nn.relu(d7),
                                                     [self.batch_size, s, s, self.output_c_dim], name='g_d8',
                                                     with_w=True)

            return tf.nn.tanh(self.d8)


    def load(self, checkpoint_dir):
        print(" [*] Reading checkpoint...")

        path = os.getcwd()
        # checkpoint_dir = '/home/cciip/文档/sketch2normal-checkpoint/checkpoint/4legs_1_256/'
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path:
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            name = os.path.join(checkpoint_dir, ckpt_name)
            # name = os.path.join(path, checkpoint_dir,ckpt_name)
            # tf.reset_default_graph()
            self.saver.restore(self.sess,name)
            return True
        else:
            return False

    def test(self, args,image_path,mask_image):

        init_op = tf.global_variables_initializer()
        self.sess.run(init_op)
        test_files = [image_path]

        n = [int(i) for i in map(lambda x: x.split('/')[-1].split('.png')[0], test_files)]
        test_files = [x for (y, x) in sorted(zip(n, test_files))]

        file = test_files[0]
        images = [self.load_data(file, mask_image, is_test=True)]

        # images = [self.load_data(file, is_test=True,mask_image) for file in test_files]

        test_images = np.array(images).astype(np.float32)
        # test_images = [test_images[i:i+self.batch_size] for i in xrange(0, len(test_images), self.batch_size)]
        test_images = [test_images[i:i + self.batch_size] for i in range(0, len(test_images), self.batch_size)]

        test_images = np.array(test_images)
        print(test_images.shape)

        if self.load(args.checkpoint_dir):
            print(" [*] Load SUCCESS")
        else:
            print(" [!] Load failed...")
            return

        print("len(test_images)", len(test_images))
        for i, test_image in enumerate(test_images):
            idx = i + 1
            print("test image " + str(idx))
            results = self.sess.run(self.fake_B_sample, feed_dict={self.real_data: test_image})
            self.save_images(results, [self.batch_size, 1],'Sketch2Normal/result.png' )

    def save_images(self, images, size, image_path):
        images = (images + 1) / 2.0
        h, w = images.shape[1], images.shape[2]
        img = np.zeros((h * size[0], w * size[1], 3))
        for idx, image in enumerate(images):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w, :] = image
        scipy.misc.imsave(image_path, img)

    def load_data(self, image_path,mask_path1, is_test=False):
        input_img = scipy.misc.imread(image_path).astype(np.float)
        index = image_path.find(self.dataset_name + '/')
        # insert_point = index + len(self.dataset_name) + 1
        # mask_path = image_path[:insert_point] + 'mask/' + image_path[insert_point:]
        print("--------------------------")
        print("mask_path", mask_path1)
        print("--------------------------")
        mask = scipy.misc.imread(mask_path1, mode='L').astype(np.float)
        img_A = input_img[:, 0:256, :]
        img_B = input_img[:, 256:512, :]

        if not is_test:
            num = random.randint(256, 286)
            img_A = scipy.misc.imresize(img_A, (num, num))
            img_B = scipy.misc.imresize(img_B, (num, num))
            mask = scipy.misc.imresize(mask, (num, num))
            num_1 = random.randint(0, num - 256)
            img_A = img_A[num_1:num_1 + 256, num_1:num_1 + 256, :]
            img_B = img_B[num_1:num_1 + 256, num_1:num_1 + 256, :]
            mask = mask[num_1:num_1 + 256, num_1:num_1 + 256]

        mask = np.reshape(mask, (256, 256, 1)) / 255.0
        img_A = img_A / 127.5 - 1.
        img_B = img_B / 127.5 - 1.
        img_AB = np.concatenate((img_A, mask, img_B), axis=2)

        return img_AB


def Sketch2NormalEstimate(imgLabel, image, mask_image):

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--dataset_name', dest='dataset_name', default='tmp', help='name of the dataset')
    parser.add_argument('--epoch', dest='epoch', type=int, default=150, help='# of epoch')
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=1, help='# images in batch')
    parser.add_argument('--output_size', dest='output_size', type=int, default=256, help='then crop to this size')
    parser.add_argument('--lr', dest='lr', type=float, default=5e-5, help='initial learning rate for optimizer')
    parser.add_argument('--phase', dest='phase', default='test', help='train, test')
    parser.add_argument('--checkpoint_dir', dest='checkpoint_dir',
                        default='Sketch2Normal/checkpoint',
                        help='models are saved here')
    parser.add_argument('--sample_dir', dest='sample_dir', default='./sample', help='sample are saved here')
    parser.add_argument('--test_dir', dest='test_dir', default='./test', help='test sample are saved here')
    parser.add_argument('--L1_lambda', dest='L1_lambda', type=float, default=500, help='weight on L1 term in objective')
    parser.add_argument('--gradient_penalty_coefficient', dest='gradient_penalty_coefficient', type=float,
                        default=100.0, help='coefficient of the gradient penalty')
    parser.add_argument('--n_critic', dest='n_critic', type=int, default=5, help='#n_critic')

    args = parser.parse_args()

    if not os.path.exists(args.checkpoint_dir):
        os.makedirs(args.checkpoint_dir)
        print("not exit ",args.checkpoint_dir)
    if not os.path.exists(args.sample_dir):
        print("not exit ",args.sample_dir)


    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        model = normalnet(sess, image_size=args.output_size, batch_size=args.batch_size,
                        output_size=args.output_size, L1_lambda=args.L1_lambda, n_critic=args.n_critic,
                        dataset_name=args.dataset_name, coefficient=args.gradient_penalty_coefficient)
        # model.test(args,image,mask_image)
        if args.phase == 'train':
            model.train(args)
        else:
            model.test(args,image,mask_image)


    imgLabel.show_picture('Sketch2Normal/result.png')

    return
# def test():
#     Sketch2NormalEstimate('', '/home/cciip/文档/sketch2normal-checkpoint/datasets/tmp/test/1.png',
#                           '/home/cciip/文档/sketch2normal-checkpoint/datasets/tmp/mask/test/1.png')


if __name__ == '__main__':
    test()