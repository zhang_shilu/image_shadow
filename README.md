# image_shadow

#### 介绍
图片阴影自动生成

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  成为仓库开发人员
2.  克隆仓库
3.  本地编写代码
4.  提交代码 git add . -> git commit -m "description" -> git pull(修改冲突) -> git push

#### pix2pix模型说明

1.  pix2pix模型文件略大，放在了网盘上。下载后放在master/GUI/pix2pix/checkpoints/facades_pix2pix目录下面即可。
2.  链接：[百度网盘](https://pan.baidu.com/s/1TnqwMLUy5fFrFtVbKKAuDw) 提取码：sihw


#### Sketch2模型说明

1.  Sketch2模型文件略大，放在了网盘上。下载后放在master/GUI/Sketch2Normal/checkpoint/目录下面即可。
2.  链接：[百度网盘](https://pan.baidu.com/s/14eRH9r_QtmS9wRwzKOZkTA)  密码: 14l3


#### faceswap模型说明

1.  直接运行master/GUI/qhw.py
2.  demo换脸：被换脸，选择master/faceswapResult/source1.png，换脸，选择master/faceswapResult/source2.png